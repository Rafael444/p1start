package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDLL;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

/**
 * Singleton class. 
 *
 *
 */
public class CarList {

	private static CarList singleton = new CarList();
	private static SortedList<Car> carList;
	
	private CarList() {
		carList = new SortedCircularDLL<Car>(new CarComparator());
	}
	
	public static CarList getInstance() {
		return singleton;
	}
	
	public  SortedList<Car> getCarList(){
		return CarList.carList;
	}
	
	public static void resetCars() {
		carList = new SortedCircularDLL<>(new CarComparator());
	}
}
