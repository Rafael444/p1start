package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

/**
 * Sorted Circular Doubly Linked List
 * 
 * Doubly Linked List implementation that
 * keeps it's elements sorted in increasing order.
 * 
 */

public class SortedCircularDLL<E> implements SortedList<E> {

	public class Node<E>{
		private Node<E> prev;
		private Node<E> next;
		private E element;

		public Node() {}

		public Node(Node<E> prev, Node<E> next, E element) {
			this.prev = prev;
			this.next = next;
			this.element = element;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public void clean() {
			this.setPrev(null);
			this.setNext(null);
			this.setElement(null);
		}
	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> cmp;

	public SortedCircularDLL(Comparator<E> cmp) {
		header = new Node<E>(header,header,null);
		currentSize = 0;
		this.cmp = cmp;
	}


	@Override
	public Iterator<E> iterator() {

		return new SortedCircularDLLIterator<>();
	}


	/**
	 * Add objects to the list in increasing order
	 * using a comparator. 
	 * @throws NullPointerException
	 * @param obj the object to add in the list,
	 * @return true when object is added to the list.
	 */
	@Override
	public boolean add(E obj) {
		nullCheck(obj);

		if(isEmpty()) {
			Node<E> newNode = new Node<E>(header, header, obj);
			header.setNext(newNode);
			header.setPrev(newNode);
			currentSize++;
			return true;
		}
		Node<E> temp = header.getNext();
		Node<E> newNode;
		while(temp != header) {
			if(cmp.compare(obj, temp.getElement()) < 0) {
				newNode = new Node<E>(temp.getPrev(),temp,obj);
				temp.getPrev().setNext(newNode);
				temp.setPrev(newNode);
				currentSize++;
				return true;
			}
			temp = temp.getNext();

		}
		newNode = new Node<E>(temp.getPrev(),temp,obj);
		temp.getPrev().setNext(newNode);
		temp.setPrev(newNode);
		currentSize++;
		return true;
	}

	/**
	 * List size.
	 * @return size of the list.
	 */
	@Override
	public int size() {

		return currentSize;
	}

	/**
	 * Remove object from the list equals to obj parameter.
	 * @throws NullPointerException
	 * @param obj the object to remove from the list,
	 * @return true when object is remove from the list.
	 * False if object was not found.
	 * 
	 */
	@Override
	public boolean remove(E obj) {
		nullCheck(obj);
		int pos = firstIndex(obj);

		return (pos < 0) ? false : remove(pos);

	}

	/**
	 * Remove object from the list in such position index.
	 * @throws IndexOutOfBoundsException
	 * @param index the position of the object to remove,
	 * @return true when object is remove from the list.
	 * 
	 */
	@Override
	public boolean remove(int index) {
		checkIndex(index);
		int i = 0;
		Node<E> temp = header;

		while(i < index) {
			i++;
			temp = temp.getNext();
		}

		Node<E> target = temp.getNext();
		temp.setNext(target.getNext());
		target.getNext().setPrev(temp);
		target.clean();
		currentSize--;


		return true;
	}


	/**
	 * Remove all objects from the list that equals obj.
	 * @throws NullPointerException
	 * @param obj the object to be removed from the list,
	 * @return  count of objects removed.
	 * 
	 */
	@Override
	public int removeAll(E obj) {
		nullCheck(obj);
		int count = 0;

		while(remove(obj)) {
			count++;
		}
		return count;
	}


	/**
	 * @return first element in the list or null if
	 * the list is empty.
	 */
	@Override
	public E first() {
		return (isEmpty()) ? null : header.getNext().getElement();
	}

	/**
	 * @return last element in the list or null if
	 * the list is empty.
	 */
	@Override
	public E last() {
		return (isEmpty()) ? null : header.getPrev().getElement();
	}

	/**
	 * @param index the position to remove.
	 * @throws IndexOutOfBoundsException
	 * @return element in given index.
	 */
	@Override
	public E get(int index) {
		checkIndex(index);

		return this.findNode(index).getElement();
	}

	/**
	 * Empty the list.
	 */
	@Override
	public void clear() {
		Node<E> temp1 = header.getNext();
		while(temp1 != header) {
			Node<E> temp2 = temp1.getNext();
			temp1.clean();
			temp1 = temp2;
		}
		header.setNext(null);
		header.setPrev(null);
		currentSize = 0;
	}

	/**
	 * @param e element to remove
	 * @throws NullPointerException
	 * @return true if  parameter e is in the list.
	 */
	@Override
	public boolean contains(E e) {
		nullCheck(e);
		return firstIndex(e) > -1;
	}

	/**
	 * @return true if the list is empty, otherwise 
	 * false. 
	 * 
	 */
	@Override
	public boolean isEmpty() {
		return currentSize == 0;
	}

	/**
	 * @param e the element to find in the list.
	 * @throws NullPointerException
	 * @return position of given parameter e.
	 * 
	 */
	@Override
	public int firstIndex(E e) {
		nullCheck(e);
		int pos = 0;

		for(E element: this) {
			if(element.equals(e)) {
				return pos;
			}
			pos++;
		}
		return -1;
	}

	/**
	 * @param e the element to find in the list.
	 * @throws NullPointerException
	 * @return last position of given parameter e.
	 * 
	 */
	@Override
	public int lastIndex(E e) {
		nullCheck(e);
		int pos = currentSize - 1;

		for(Node<E> temp = header.getPrev(); temp != header; temp = temp.getPrev()) {
			if(temp.getElement().equals(e)) {
				return pos;
			}
			pos--;
		}

		return -1;
	}

	/**
	 * Search node in the list.
	 * @throws IndexOutOfBoundsException
	 * @param index the position of the node.
	 * @return node in position index.
	 */
	public Node<E> findNode(int index){
		checkIndex(index);
		int i = 0;
		Node<E> temp = header.getNext();

		while(i < index) {
			temp = temp.getNext();
			i++;
		}
		return temp;
	}
	
	
	/**
	 * Converts the list in an array.
	 * @return array
	 */
	public Car[] toArray() {
		Car[] arr = new Car[this.size()];
		int i = 0;
		for(E e: this) {
			arr[i] = (Car) e;
			i++;
		}
		return arr;
	}

	/**
	 * Check if the index is valid
	 * @param index the position to check.
	 */
	public void checkIndex(int index) {
		if(index < 0 || index >= currentSize) {
			throw new IndexOutOfBoundsException();
		}
	}

	/**
	 * Check if the parameter e is null.
	 * @param e the object to check.
	 */
	public void nullCheck(E e) {
		if(e == null ) {
			throw new NullPointerException("Parameter can't be null");
		}
	}

	/**
	 * Iterator class to iterate through the list.
	 */
	public  class SortedCircularDLLIterator<E> implements Iterator<E>{
		private Node<E> current;

		public SortedCircularDLLIterator() {
			current = (Node<E>) header.getNext();
		}


		@Override
		public boolean hasNext() {

			return current != header;
		}

		@Override
		public E next() {
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			E result = current.getElement();
			current = current.getNext();
			return result;
		}

	}



}
