package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDLL;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

/**
 * Rest API implementation class.
 */

@Path("/cars")
public class CarManager {
	private SortedList<Car> carList = CarList.getInstance().getCarList();

	/**
	 * 
	 * @return all cars in the remote server.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {


		return ((SortedCircularDLL<Car>)carList).toArray();
	}

	/**
	 * 
	 * @param id the parameter to match with car id.
	 * @return car with match id.
	 * @throws WebApplicationException(404)
	 * 
	 */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){


		for(Car c: carList) {
			if(c.getCarId() == id) {
				return c;
			}
		}


		throw new WebApplicationException(404);

	}

	/**
	 * Adds car to remote server.
	 * @param car the car to add to the server.
	 * @return response status 201 if was added to the list, 
	 * otherwise return conflict if the car parameter is null or one of it's fields, or if 
	 * there is a car with the same id in the list.
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		if(car == null || car.getCarBrand() == null || car.getCarModel() == null || car.getCarModelOption() == null) {
			return Response.status(Response.Status.CONFLICT).build();
		}

		if(carList.isEmpty()) {
			carList.add(car);
			return Response.status(201).build();
		}


		for(Car c: carList) {
			if(c.getCarId() == car.getCarId()) {
				return Response.status(Response.Status.CONFLICT).build();
			}
		}

		carList.add(car);
		return Response.status(201).build();
	}


	/**
	 * Change fields of a car object in the server.
	 * @param car the parameter to change it's fields.
	 * @return ok if car is found, otherwise returns not found.
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){

		for(Car c: carList) {
			if (c.getCarId() == car.getCarId()) {
				carList.remove(c);
				carList.add(car);

				return Response.status(Response.Status.OK).build();
			} 

		}
		return Response.status(Response.Status.NOT_FOUND).build();      

	}  

	/**
	 * Remove car from remote server.
	 * @param id the id of the car to remove
	 * @return ok if car is deleted, otherwise returns 
	 * not found.
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){

		for(Car c: carList) {
			if(c.getCarId() == id) {
				carList.remove(c);
				return Response.status(Response.Status.OK).build();
			}
		}

		return Response.status(Response.Status.NOT_FOUND).build();
	}






}
